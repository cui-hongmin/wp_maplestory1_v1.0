package com.neutech.maplestory.constant;

 /**
  * 存放项目中的所有的常量
  * */
public class Constant {
    /**
     * 游戏窗口的宽度
     */
    public static final int GAME_WIDTH = 1350;
    /**
     * 游戏窗口的宽度
     */
    public static final int GAME_HEIGHT = 1000;
    /**
     * 游戏的标题
     */
    public static final String GAME_TITLE = "王牌班——MapleStory";

    /**
     * 图片路径的前缀
     */
    public static final String IMAGE_PATH_PRE = "com/neutech/maplestory/images/";
    /**
     * 图片路径的后缀
     */
    public static final String IMAGE_PATH_FIX = ".png";
    /**
     * 英雄的初始速度
     */
    public static final int HERO_SPEED = 10;
    /**
     * 初始的跳跃速度
     */
    public static final double INIT_JUMP_SPEED = 40;

     /**
      * 道具初始的跳跃速度
      */
     public static final double INIT_ITEM_JUMP_SPEED = 25;


 }