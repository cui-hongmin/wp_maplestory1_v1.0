package com.neutech.maplestory.util;

import com.neutech.maplestory.constant.Constant;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

/**
 * 加载音频、图片等静态资源的工具类
 */
public class GameUtil {
    private GameUtil(){}

    /**
     * 通过图片名获取图片对象
     * @param imageName 图片名称
     * @return 图片对象
     */
    public static Image getImage(String imageName){
        URL url = GameUtil.class.getClassLoader().getResource(Constant.IMAGE_PATH_PRE + imageName + Constant.IMAGE_PATH_FIX);
        BufferedImage img = null;
        try {
            img = ImageIO.read(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return img;
    }
}
