package com.neutech.maplestory.util;

import com.neutech.maplestory.constant.Constant;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * 项目中，实现窗口的父类，项目中的主类继承该父类
 */
public class MapleStoryFrame extends Frame {
    public MapleStoryFrame(){}

    /**
     * 初始化窗口的方法
     */
    public void init(){
        this.setIconImage(ImageUtil.getImage("logo"));
        this.setSize(Constant.GAME_WIDTH,Constant.GAME_HEIGHT);
        this.setVisible(true);
        this.setResizable(false);
        this.setTitle(Constant.GAME_TITLE);

        this.setLocationRelativeTo(null);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        // 启动重画线程
        new MapleStoryThread().start();
    }

    /**
     * 实现窗口重新绘制的线程内部类
     */
    class MapleStoryThread extends Thread{
        @Override
        public void run() {
            for(;;){
                repaint();
                try {
                    Thread.sleep(40);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    /**
     * 解决图片闪烁的问题，用双缓冲方法解决闪烁问题
     */
    private Image backImg = null;

    /**
     * 重写update()方法，在窗口的里层添加一个虚拟的图片
     * @param g 画笔
     */
    @Override
    public void update(Graphics g) {
        if (backImg == null) {
            // 如果虚拟图片不存在，创建一个和窗口一样大小的图片
            backImg = createImage(Constant.GAME_WIDTH, Constant.GAME_HEIGHT);
        }
        // 获取到虚拟图片的画笔
        Graphics backg = backImg.getGraphics();
        Color c = backg.getColor();
        backg.setColor(Color.WHITE);
        backg.fillRect(0, 0, Constant.GAME_WIDTH, Constant.GAME_HEIGHT);
        backg.setColor(c);
        // 调用虚拟图片的paint()方法，每50ms刷新一次
        paint(backg);
        g.drawImage(backImg, 0, 0, null);
    }
}