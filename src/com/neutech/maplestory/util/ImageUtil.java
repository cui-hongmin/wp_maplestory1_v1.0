package com.neutech.maplestory.util;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * 静态加载项目中所有图片资源
 */
public class ImageUtil {
    private static Map<String, Image> imagesMap = new HashMap<>();
    static {
        // LOGO
        imagesMap.put("logo",GameUtil.getImage("icon/logo"));
        // hero right stand
        for (int i = 0; i < 4; i++) {
            imagesMap.put("hero_right_stand" + i,GameUtil.getImage("hero/right/stand/stand1_"+i));
        }
        // hero left stand
        for (int i = 0; i < 4; i++) {
            imagesMap.put("hero_left_stand" + i,GameUtil.getImage("hero/left/stand/stand1_"+i));
        }

        // hero right walk
        for (int i = 0; i < 5; i++) {
            imagesMap.put("hero_right_walk" + i,GameUtil.getImage("hero/right/walk/walk1_"+i));
        }
        // hero left walk
        for (int i = 0; i < 5; i++) {
            imagesMap.put("hero_left_walk" + i,GameUtil.getImage("hero/left/walk/walk1_"+i));
        }
        // hero left right prone
        imagesMap.put("hero_right_prone",GameUtil.getImage("hero/right/prone/prone_0"));
        imagesMap.put("hero_left_prone",GameUtil.getImage("hero/left/prone/prone_0"));
        // hero left right jump
        imagesMap.put("hero_right_jump",GameUtil.getImage("hero/right/jump/jump_0"));
        imagesMap.put("hero_left_jump",GameUtil.getImage("hero/left/jump/jump_0"));
        // hero right shoot
        for (int i = 0; i < 4; i++) {
            imagesMap.put("hero_right_shoot" + i,GameUtil.getImage("hero/right/shoot/shoot1_"+i));
        }
        // hero left shoot
        for (int i = 0; i < 4; i++) {
            imagesMap.put("hero_left_shoot" + i,GameUtil.getImage("hero/left/shoot/shoot1_"+i));
        }
        //hero left right arrow
        imagesMap.put("hero_right_arrow",GameUtil.getImage("arrow/right/hero_arrow"));
        imagesMap.put("hero_left_arrow",GameUtil.getImage("arrow/left/hero_arrow"));
        for (int i = 0; i < 3; i++) {
            imagesMap.put("mob01_left_stand" + i,GameUtil.getImage("mob/mob01/left/stand/"+i));
        }
        for (int i = 0; i < 3; i++) {
            imagesMap.put("mob01_left_walk" + i,GameUtil.getImage("mob/mob01/left/walk/"+i));

        }
        for (int i = 0; i < 12; i++) {
            imagesMap.put("mob01_left_die" + i,GameUtil.getImage("mob/mob01/left/die/"+i));

        }
        //普通小怪的血条
        imagesMap.put("blood",GameUtil.getImage("blood/blood"));
        //道具
        imagesMap.put("HP_50",GameUtil.getImage("item/HP_50"));
        imagesMap.put("HP_100",GameUtil.getImage("item/HP_100"));
        imagesMap.put("HP_300",GameUtil.getImage("item/HP_300"));
        imagesMap.put("MP_100",GameUtil.getImage("item/MP_100"));
        //道具包
        imagesMap.put("itemPackage",GameUtil.getImage("itempackage/ItemPackage"));
        // 伤害值图片组
        for (int i = 0; i < 10; i++) {
            imagesMap.put("power" +i,GameUtil.getImage("power/" + i));


        }
    }

    private ImageUtil(){}

    /**
     * 通过图片对象的key获取图片对象
     * @param key 键
     * @return 图片对象
     */
    public static Image getImage(String key){
        return imagesMap.get(key);
    }

}
