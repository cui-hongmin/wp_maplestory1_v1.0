
package com.neutech.maplestory.entity;

import com.neutech.maplestory.client.MapleStoryClient;

import java.awt.*;

/**
 * 冒险岛项目中的所有实体类的父类
 * */
public abstract class AbstractMapleStoryObject implements Moveable,Drawable{
    /**
     * 横坐标
     */
    public int x;
    /**
     * 纵坐标
     */
    public int y;
    /**
     * 实体类对象图片组
     */
    public Image[] imgs;
    /**
     * 实体类的速度
     */
    public int speed;
    /**
     * 宽度
     */
    public int width;
    /**
     * 高度
     */
    public int height;
    /**
     * 表示方向的枚举类型
     */
    public Direction dir;
    /**
     * 表示动作的枚举类型
     */
    public Action action;
    /**
     * 项目中的中介者(管家)
     * */
    public MapleStoryClient msc;
    /**
     * 表示生死变量
     * */
    public boolean live;
    /**
     * 表示血量的变量
     * */
    public int HP;
    @Override
    public void move() {}
    /**
     * 获取矩形对象的方法
     * @return 矩形对象
     * */

    public Rectangle getRectangle(){
        return new Rectangle(x,y,width,height);

    }

}