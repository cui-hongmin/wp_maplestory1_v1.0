package com.neutech.maplestory.entity;

import com.neutech.maplestory.client.MapleStoryClient;
import com.neutech.maplestory.constant.Constant;
import com.neutech.maplestory.util.ImageUtil;

import java.awt.*;
import java.util.List;
import java.util.Random;

public class Arrow extends AbstractMapleStoryObject{
    public static Image[] imgs = {
            ImageUtil.getImage("hero_right_arrow"),
            ImageUtil.getImage("hero_left_arrow"),
    };
    /**
     * 表示生死变量
     * */
    public boolean live;
    public Arrow(){
        this.speed = 20;
        this.live = true;
        this.width = imgs[0].getWidth(null);
        this.height = imgs[0].getHeight(null);
    }
    public Arrow(MapleStoryClient msc,int x, int y, Direction dir){
        this();
        this.msc = msc;
        this.x = x;
        this.y = y;
        this.dir = dir;

    }
    @Override
    public void draw(Graphics g) {
        if (!live){
            msc.arrows.remove(this);
            return;
        }
        switch (dir){
            case RIGHT:
                g.drawImage(imgs[0],x,y,null);
                break;
            case LEFT:
                g.drawImage(imgs[1],x,y,null);
                break;
            default:
                break;
        }
        move();

    }

    @Override
    public void move() {
        super.move();
        switch (dir){
            case RIGHT:
                this.x += speed;
                break;
            case LEFT:
                this.x -= speed;
                break;
            default:
                break;
        }
        outOfBound();
    }
    //边界出界判断
    private void outOfBound(){
        if (this.x < -500 || this.x > Constant.GAME_WIDTH + 500){
            this.live = false;
        }
    }
    /**
     * 随机数生成器
     * */
    public static Random r = new Random();
    /**
     * 弓箭hit怪物的方法
     * @return mob 被击中的怪
     * @return 是否击中
     * */
    private boolean hit(Mob mob) {
        if (this.live && this.getRectangle().intersects(mob.getRectangle()) && mob.action != Action.DIE){
            this.live = false;
//            msc.hero.hitValue = r.nextInt( msc.hero.ATT - 5) + 10;  //取值范围 8 ~12
            msc.hero.hitValue =(int)(msc.hero.ATT * (1 + r.nextDouble()));

            //创建出伤害值
            Power power = new Power(msc,mob.x,mob.y);
            msc.powers.add(power);
            mob.HP -= power.value;

            if (mob.HP <= 0){
                mob.action = Action.DIE;
                if (r.nextInt(100)< 100){
                    Item item = new Item(msc,mob.x + mob.width/2 - 14,mob.y + mob.height - 30,r.nextInt(4));
                    msc.items.add(item);
                }

            }

//            mob.live = false;
            return true;
        }
        return false;
    }
    /**
     * 一只弓箭与所有的怪物进行碰撞检测的方法
     * @return mobs 怪物容器
     * @return 是否击中
     * */
    public boolean hit(List<Mob> mobs){
        for (int i = 0; i < mobs.size(); i++) {
            Mob mob = mobs.get(i);
            if (hit(mob)){
                return true;
            }
        }
        return false;
    }
}
