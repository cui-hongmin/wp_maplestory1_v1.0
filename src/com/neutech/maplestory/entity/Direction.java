package com.neutech.maplestory.entity;

/**
 * 表示方向的枚举类型
 *
 */
public enum Direction {
    LEFT,RIGHT
}