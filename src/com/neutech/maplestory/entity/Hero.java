package com.neutech.maplestory.entity;

import com.neutech.maplestory.client.MapleStoryClient;
import com.neutech.maplestory.constant.Constant;
import com.neutech.maplestory.util.ImageUtil;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Random;


public class Hero extends AbstractMapleStoryObject{
    public static Image[] imgs = new Image[100];
    static {
        for (int i = 0; i < 4; i++) {
            imgs[i] = ImageUtil.getImage("hero_right_stand" + i);
        }
        for (int i = 4; i < 8; i++) {
            imgs[i] = ImageUtil.getImage("hero_left_stand" + (i - 4));
        }
        for (int i = 8; i < 13; i++) {
            imgs[i] = ImageUtil.getImage("hero_right_walk" + (i - 8));
        }
        for (int i = 13; i < 18; i++) {
            imgs[i] = ImageUtil.getImage("hero_left_walk" + (i - 13));
        }

        imgs[18] = ImageUtil.getImage("hero_right_prone");
        imgs[19] = ImageUtil.getImage("hero_left_prone");
        imgs[20] = ImageUtil.getImage("hero_right_jump");
        imgs[21] = ImageUtil.getImage("hero_left_jump");
        for (int i = 22; i < 26; i++) {
            imgs[i] = ImageUtil.getImage("hero_right_shoot" + (i - 22));
        }
        for (int i = 26; i < 30; i++) {
            imgs[i] = ImageUtil.getImage("hero_left_shoot" + (i - 26));
        }


    }
    public MapleStoryClient msc;

    public Hero(){}
    /**
     * 人物血量上线
     * */
    public int MAX_MP = 1000;
    public int MP;
    /**
     * 人物魔法值上线
     * */
    public int MAX_HP = 1000;
    public int HP;
    /**
     *基础攻击力
     * */
    public int ATT;
    /**
     * 伤害值
     * */
    public int hitValue;
    public Random r = new Random();

    public Hero(MapleStoryClient msc,int x,int y){
        this.msc = msc;
        this.x = x;
        this.y = y;
        this.MP = MAX_MP;
        this.HP= MAX_HP;
        this.ATT = 1000;

        this.speed = Constant.HERO_SPEED;
        this.width = imgs[0].getWidth(null);
        this.height = imgs[0].getHeight(null);
        this.dir = Direction.RIGHT;
        this.action = Action.STAND;
    }

    /**
     * 一直加的计数器
     */
    public int count = 0;
    /**
     * 计算当前画第几张图
     */
    private int step = 0;
    /**
     * 左右的开关
     */
    public boolean left,right,down,jump,shoot,pickup;
    @Override
    public void draw(Graphics g) {
        switch (dir){
            case RIGHT:
                switch (action){
                    case STAND:
                        // right stand
                        if(count++ % 3 == 0){
                            g.drawImage(imgs[step++ % 4],x,y,null);
                        }else {
                            g.drawImage(imgs[step% 4],x,y,null);
                        }
                        break;
                    case WALK:
                        // right walk
                        if(count++ % 3 == 0){
                            g.drawImage(imgs[(step++ % 5) + 8],x,y,null);
                        }else {
                            g.drawImage(imgs[step % 5 + 8],x,y,null);
                        }
                        break;
                    case PRONE:
                        g.drawImage(imgs[18],x + 10,y + 32,null);
                        break;
                    case JUMP:
                        g.drawImage(imgs[20],x,y,null);
                        break;
                    case SHOOT:
                        // right shoot
                        if(count++ % 3 == 0){
                            g.drawImage(imgs[step++ % 4 + 22],x,y,null);
                        }else {
                            g.drawImage(imgs[step% 4 + 22],x,y,null);
                        }
                        break;
                    default:
                        break;
                }
                break;
            case LEFT:
                switch (action){
                    case STAND:
                        // left stand
                        if(count++ % 3 == 0){
                            g.drawImage(imgs[(step++ % 4) + 4],x,y,null);
                        }else {
                            g.drawImage(imgs[step % 4 + 4],x,y,null);
                        }
                        break;
                    case WALK:
                        // left walk
                        if(count++ % 3 == 0){
                            g.drawImage(imgs[(step++ % 5) + 13],x,y,null);
                        }else {
                            g.drawImage(imgs[step % 5 + 13],x,y,null);
                        }
                        break;
                    case PRONE:
                        g.drawImage(imgs[19],x - 8,y + 32,null);
                        break;
                    case JUMP:
                        g.drawImage(imgs[21],x,y,null);
                        break;
                    case SHOOT:
                        // left shoot
                        if(count++ % 3 == 0){
                            g.drawImage(imgs[step++ % 4 + 26],x,y,null);
                        }else {
                            g.drawImage(imgs[step% 4 + 26],x,y,null);
                        }
                        break;
                    default:
                        break;
                }

                break;
            default:
                break;
        }


        move();
    }

    @Override
    public void move() {
        if(left){
            x -= speed;
            this.action = Action.WALK;
        }
        if(right){
            x += speed;
            this.action = Action.WALK;
        }
        if(jump){
            jump();
        }
        if (shoot){
            shoot();
        }
        if (pickup){
            pickup(msc.items);
        }

        confirmStatus();
        outOfBound();
    }
    /**
     * 射击持续时长的变量
     * */
    private int shootCount = 0;
    /**
     * 射击的方法
     * */
    private void shoot() {
        shootCount++;

        if (shootCount % 10 ==0){

            shoot = false;
            this.MP -= 5;
            Arrow arrow;
            if (dir == Direction.RIGHT){
                 arrow = new Arrow(msc,x + width,y + height /2 + 10,dir);
            }else {
                arrow = new Arrow(msc,x,y + height /2 + 10,dir);
            }

            msc.arrows.add(arrow);
        }
    }

    private double v0 = Constant.INIT_JUMP_SPEED;
    private double vt;
    private double g = 9.8;
    private double delta_height;
    private double t = 0.5;
    /**
     * 跳跃的方法
     */
    private void jump() {
        // vt = v0 - gt 竖直上抛
        vt = v0 - g * t;
        delta_height = v0 * t;
        v0 = vt;
        y -= delta_height;
        // 自由落体的结束位置
        if(y >= 500){
            jump = false;
            v0 = Constant.INIT_JUMP_SPEED;
            vt = 0.0;
            y = 500;
        }

    }

    /**
     * 根据当前人物的左右开关，确定人物的方向
     */
    private void confirmStatus(){
        if(left && !right){
            this.dir = Direction.LEFT;
            if(jump){
                this.action = Action.JUMP;
            }

        }else if(right && !left){
            this.dir = Direction.RIGHT;
            if(jump){
                this.action = Action.JUMP;
            }
        }else if(jump){
            this.action = Action.JUMP;
        }else if(down){
            this.action = Action.PRONE;
        }else if (shoot){
                this.action = Action.SHOOT;
            }
        // 都按下或者都不按下
        else{
            this.action = Action.STAND;
        }
    }

    /**
     * 按键的方法
     * @param e 键盘事件对象
     */
    public void keyPressed(KeyEvent e){
        switch (e.getKeyCode()){
            case KeyEvent.VK_A:
                left = true;
                break;
            case KeyEvent.VK_D:
                right = true;
                break;
            case KeyEvent.VK_S:
                down = true;
                break;
            case KeyEvent.VK_K:// 跳跃
                jump = true;
                break;
            case KeyEvent.VK_J:// 射击
                shoot = true;
                break;
            case KeyEvent.VK_L:// 拾取
                pickup = true;
                break;
            case KeyEvent.VK_I: //道具包

                break;
            default:
                break;
        }
    }

    /**
     * 释放键的方法
     * @param e 键盘事件
     */
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()){
            case KeyEvent.VK_A:
                left = false;
                break;
            case KeyEvent.VK_D:
                right = false;
                break;
            case KeyEvent.VK_S:
                down = false;
                break;
            /*case KeyEvent.VK_J:
                shoot = false;
                break;*/
            case KeyEvent.VK_L:// 拾取
                pickup = false;
                break;
            default:
                break;
        }
    }
    /**
     * 人物出界
     * */
    private void outOfBound(){
        if (this.x <= 0 ){
            this.x = 0;
        }
        if (this.x >= Constant.GAME_WIDTH - this.width){
            this.x =  Constant.GAME_WIDTH - this.width;
        }
    }
    /**
     * 拾取道具的方法
     * @return item 被拾取的道具对象
     * @return 是否捡取到
     * */
    private boolean pickup(Item item){
        if (item.live && this.getRectangle().intersects(item.getRectangle())){
            item.live = false;
            //存放之前和道具包里面的每一个进行判断
            for (int i = 0; i < msc.itemPackage.items.size(); i++) {
                Item itm = msc.itemPackage.items.get(i);
                //如果相同,数量加一
                if (item.type == itm.type){
                    itm.qty += 1;
                    return false;
                }
            }
            // 业务 放入道具包
            msc.itemPackage.items.add(item);

           /* switch (item.type){
                case 0:
                    this.HP += 50;
                    break;
                case 1:
                    this.HP += 100;
                    break;
                case 2:
                    this.HP += 300;
                    break;
                case 3:
                    this.MP += 100;
                    break;
                default:
                    break;
            }
            if (HP >= MAX_HP){
                HP = MAX_HP;
            }
            if (MP >= MAX_MP){
                MP = MAX_MP;
            }*/


        }
        return false;
    }
    /**
     *拾取一堆道具
     * @return items 道具容器
     * @return 是否拾取到
     * */
    public boolean pickup(List<Item>items){
        for (int i = 0; i < items.size(); i++) {
            Item item = items.get(i);
            if (pickup(item)){
                return true;
            }

        }
        return false;
    }
}