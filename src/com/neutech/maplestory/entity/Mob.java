package com.neutech.maplestory.entity;

import com.neutech.maplestory.client.MapleStoryClient;
import com.neutech.maplestory.util.GameUtil;
import com.neutech.maplestory.util.ImageUtil;

import java.awt.*;

public class Mob extends AbstractMapleStoryObject{
    public static Image[] imgs = new Image[100];
    static {
        for (int i = 0; i < 3; i++) {
            imgs[i] = ImageUtil.getImage("mob01_left_stand"+i);
        }
        for (int i = 3; i < 6; i++) {
            imgs[i] = ImageUtil.getImage("mob01_left_walk" + (i - 3));
        }
        for (int i = 6; i < 18; i++) {
            imgs[i] = ImageUtil.getImage("mob01_left_die" + (i - 6));
        }
    }
    /**
     * 最大血量
     * */
    public int MAX_HP;
    public Mob (){
        this.width = imgs[0].getWidth(null);
        this.height = imgs[0].getHeight(null);
        this.dir = Direction.LEFT;
        this.action = Action.STAND;
        this.live = true;
        this.MAX_HP = 10000;
        this.HP = MAX_HP;

    }
    public Mob( MapleStoryClient msc,int x, int y,int speed){
        this();
        this.msc = msc;
        this.x = x;
        this.y = y;
        this.speed = speed;

    }
    private int count;
    private int step;
    private int diestep;
    @Override
    public void draw(Graphics g) {
        if (diestep >= 11){
            this.live = false;
            diestep = 0;
        }
        if (!live){
            msc.mobs.remove(this);
            return;
        }
        switch (dir){
            case LEFT:
                switch (action){
                    case STAND:
                        if (count++ % 3 == 0){
                            g.drawImage(imgs[step++ % 3],x,y,null);
                        }else {
                            g.drawImage(imgs[step++ % 3],x,y,null);
                        }
                        break;
                    case WALK:
                        if (count++ % 3 == 0){
                            g.drawImage(imgs[step++ % 3 + 3],x,y,null);
                        }else {
                            g.drawImage(imgs[step++ % 3 + 3],x,y,null);
                        }
                        break;
                    case DIE:
                        if (count++ % 3 == 0){
                            g.drawImage(imgs[diestep++ % 12 + 6],x,y,null);
                        }else {
                            g.drawImage(imgs[diestep++ % 12 + 6],x,y,null);
                        }
                        break;
                    default:
                        break;
                }
                break;
        }
        if (HP < MAX_HP){
            bloodBar.draw(g);
        }

        move();

    }

    @Override
    public void move() {
        switch (dir){
            case LEFT:
                switch (action){
                    case WALK:
                    this.x -= speed;
                    break;
                    default:
                        break;
                }
                break;
            case RIGHT:
                this.x += speed;
                break;
            default:
                break;
        }
    }
    public BloodBar bloodBar = new BloodBar();
    class BloodBar extends AbstractMapleStoryObject{
        private Image img;
        public BloodBar (){
            this.img = ImageUtil.getImage("blood");
            this.width = img.getWidth(null);

        }

    @Override
    public void draw(Graphics g) {
         /*   //框
        //内部类访问外部类相同属性名时，使用外部类。this.属性名
        Color c = g.getColor();
            g.drawRect(Mob.this.x,Mob.this.y - 15,Mob.this.width,10);
            //填充
        g.setColor(Color.GREEN);
        g.fillRect(Mob.this.x + 1,Mob.this.y - 15 +1,(int)(Mob.this.width * Mob.this.HP / Mob.this.MAX_HP) , 10 - 1);
        g.setColor(c);*/
        int n = Mob.this.width/this.width * Mob.this.HP / Mob.this.MAX_HP;
        for (int i= 0;i <n;i++){
            g.drawImage(img,Mob.this.x + (i * this.width),Mob.this.y - 15,null);
        }

    }
    }
}
