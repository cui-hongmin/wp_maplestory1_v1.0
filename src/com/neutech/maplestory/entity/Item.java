package com.neutech.maplestory.entity;

import com.neutech.maplestory.client.MapleStoryClient;
import com.neutech.maplestory.constant.Constant;
import com.neutech.maplestory.util.ImageUtil;

import java.awt.*;

public class Item extends AbstractMapleStoryObject{
    public Item(){}
    /**
     * 道具类型
     * */
    public int type;
    public Image img;

    public boolean jump;
    /**
     * 当前道具的数量
     * */
    public int qty = 1;
    public Item(MapleStoryClient msc,int x, int y,int type){
        this.msc= msc;
        this.x = x;
        this.y = y;
        this.jump = true;
        this.live = true;
        this.type = type;
        switch (type){
            case 0:
                this.img = ImageUtil.getImage("HP_50");
            break;
            case 1:
                this.img = ImageUtil.getImage("HP_100");
                break;
            case 2:
                this.img = ImageUtil.getImage("HP_300");
                break;
            case 3:
                this.img = ImageUtil.getImage("MP_100");
                break;
            default:
                break;

        }
        this.width = 25;
        this.height = 25;


    }
    @Override
    public void draw(Graphics g) {
        if (!live){
            msc.items.remove(this);
            return;
        }
        g.drawImage(img,x,y,null);
        move();
    }

    @Override
    public void move() {
        if (jump){
            jump();
        }
    }

    private double v0 = Constant.INIT_ITEM_JUMP_SPEED;
    private double vt;
    private double g = 9.8;
    private double delta_height;
    private double t = 0.7;
    /**
     * 跳跃的方法
     */
    private void jump() {
        // vt = v0 - gt 竖直上抛
        vt = v0 - g * t;
        delta_height = v0 * t;
        v0 = vt;
        y -= delta_height;
        // 自由落体的结束位置
        if(y >= 550){
            jump = false;
            v0 = Constant.INIT_ITEM_JUMP_SPEED;
            vt = 0.0;
            y = 550;
        }

    }
}
