package com.neutech.maplestory.entity;

/**
 * 项目中所有的类都要实现该移动的接口<br>

 */
public interface Moveable {
    void move();
}