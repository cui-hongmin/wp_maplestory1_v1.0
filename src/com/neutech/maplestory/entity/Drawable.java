package com.neutech.maplestory.entity;

import java.awt.*;

/**
 * 项目中所有的类都要实现画的接口<br>
 */
public interface Drawable {
    /**
     * 所有的类都要重写该画的方法
     * @param g 画笔
     */
    void draw(Graphics g);
}