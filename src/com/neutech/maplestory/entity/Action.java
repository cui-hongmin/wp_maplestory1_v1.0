package com.neutech.maplestory.entity;

/**
 * 表示动作的枚举类型
 *
 */
public enum Action {
    STAND,WALK,PRONE,JUMP,SHOOT,DIE
}