package com.neutech.maplestory.entity;

import com.neutech.maplestory.client.MapleStoryClient;
import com.neutech.maplestory.util.ImageUtil;

import java.awt.*;

public class Power extends AbstractMapleStoryObject{
    public static Image[] imgs = new Image[10];
    static {
        for (int i = 0; i < 10; i++) {
            imgs[i] = ImageUtil.getImage("power" + i);
        }
    }
    public Power (){
        this.speed = 4;
        this.live = true;

    }
    /**
     * 每一个伤害值
     * */
    public int value;
    public Power (MapleStoryClient msc,int x,int y){
        this();
        this.msc= msc;
        this.x = x;
        this.y = y;
        this.value = msc.hero.hitValue;
    }
    @Override
    public void draw(Graphics g) {
        if (!live){
            msc.powers.remove(this);
            return;
        }
        drawValue(g);
       /* Font f = g.getFont();
        g.setFont(new Font("微软雅黑",Font.BOLD,40));
        g.drawString(value + "",x,y);
        g.setFont(f);*/
        move();
    }
    /**
     * 使用图片画伤害值
     * @param g 画笔
     * */
    public void drawValue(Graphics g){
         int power= this.value;  //  假设其为1728
        if (power >= 9999){
            power = 9999;
        }
        int units = power % 10; //  拿到8
        power = power / 10;  //拿到172
        int tens = power % 10;  //拿到2
        power = power / 10;    //17
        int hundreds = power % 10;  //拿到7
        power = power / 10;  //1
        int thousands = power;   //
        if (tens == 0 && hundreds == 0 && thousands == 0 && units != 0){
            //只画个位
            g.drawImage(imgs[units],x,y,null);
        }
       if (hundreds == 0 && thousands == 0 && tens != 0){
           //画十位
           g.drawImage(imgs[tens],x,y,null);
           //画个位
           g.drawImage(imgs[units],x + 30,y,null);
       }
        if (hundreds != 0 && thousands == 0){
            //画百位
            g.drawImage(imgs[hundreds],x,y,null);
            //画十位
            g.drawImage(imgs[tens],x + 30,y,null);
            //画个位
            g.drawImage(imgs[units],x + 60,y,null);
        }
        if (thousands != 0){
            //画千位
            g.drawImage(imgs[thousands],x, y,null);
            //画百位
            g.drawImage(imgs[hundreds],x + 30,y,null);
            //画十位
            g.drawImage(imgs[tens],x + 60,y,null);
            //画个位
            g.drawImage(imgs[units],x + 90,y,null);
        }
    }

    @Override
    public void move() {
        if (this.y <= 300){
            this.live = false;
        }
        this.y -= speed;
    }
}
