package com.neutech.maplestory.entity;

import com.neutech.maplestory.client.MapleStoryClient;
import com.neutech.maplestory.util.ImageUtil;

import java.awt.*;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ItemPackage extends AbstractMapleStoryObject{
    public List<Item> items = new CopyOnWriteArrayList<>();
    public Image img;
    public ItemPackage() {
        this.x = 1100;
        this.y = 350;
    }
    public ItemPackage(MapleStoryClient msc){
        this();
        this.msc = msc;
        this.img = ImageUtil.getImage("itemPackage");
       /* this.x = x;
        this.y = y;*/

    }

    @Override
    public void draw(Graphics g) {
        if (live){
            g.drawImage(img,x,y,null);
            for (int i = 0; i < items.size(); i++) {
                Item item = items.get(i);
                g.drawImage(item.img,x +(i*35) + 15,y + 50,null);
                g.drawString(item.qty + "",x + (i*35) + 13,y + 70);

            }
        }

    }
}
