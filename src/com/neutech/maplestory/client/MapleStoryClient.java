package com.neutech.maplestory.client;

import com.neutech.maplestory.entity.*;
import com.neutech.maplestory.util.MapleStoryFrame;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 游戏的主入口文件
 * */

public class MapleStoryClient extends MapleStoryFrame {
    public Hero hero = new Hero(this,300,500);
    public List<Arrow> arrows = new CopyOnWriteArrayList<>();
    public List<Mob> mobs = new CopyOnWriteArrayList<>();

    public List<Item> items=  new CopyOnWriteArrayList<>();
    public List<Power> powers=  new CopyOnWriteArrayList<>();

    public ItemPackage itemPackage = new ItemPackage(this);
    {
        for (int i = 0; i < 10; i++) {
            Mob mob = new Mob (this,550 + (i * 80),520,5);
            mobs.add(mob);
        }
    }


    @Override
    public void init() {
        super.init();
        // 添加键盘监听器
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {

                hero.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_I) {
                    // 开关切换
                    itemPackage.live = !itemPackage.live;
                }
            }
            @Override
            public void keyReleased(KeyEvent e){
                hero.keyReleased(e);

            }
        });
    }

    @Override
    public void paint(Graphics g) {
        hero.draw(g);
//        arrow.draw(g);
        //使用迭代器和CopyOnWriteArrayList解决数组索引移动（非线程安全的问题）
        Iterator<Arrow> itArrows = arrows.iterator();
        while (itArrows.hasNext()){
            Arrow arrow = itArrows.next();
            arrow.draw(g);
            arrow.hit(mobs);
        }

        /*for (int i = 0; i < arrows.size(); i++) {
            Arrow arrow = arrows.get(i);
            arrow.draw(g);
            arrow.hit(mobs);
        }*/
        Iterator<Mob> itMobs = mobs.iterator();
        while (itMobs.hasNext()){
            Mob mob= itMobs.next();
            mob.draw(g);
        }
        Iterator<Power> itPowers = powers.iterator();
        while (itPowers.hasNext()){
            Power power = itPowers.next();
            power.draw(g);
        }

        Iterator<Item> itItems = items.iterator();
        while (itItems.hasNext()){
           Item item = itItems.next();
            item.draw(g);
        }
        itemPackage.draw(g);

       /* for (int i = 0; i < mobs.size(); i++) {
            mobs.get(i).draw(g);
        }*/


        Font f = g.getFont();
        g.setFont(new Font("微软雅黑",Font.BOLD,30));
        g.drawString("人物的方向为：" + hero.dir,100,100);
        g.drawString("人物的动作为：" + hero.action,100,150);
        g.drawString("人物的HP：" + hero.HP,100,250);
        g.drawString("人物的MP：" + hero.MP,100,300);
        g.setFont(f);
    }

    public static void main(String[] args) {
        new MapleStoryClient().init();

    }
}